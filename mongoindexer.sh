#!/bin/bash
###
#   script to indexate the collections depending if are for visit_promos or visit_mcp
#   To considerate  inside the same folder you execute the script should exist the files
#   "promos_without_bson"  and "mcp_without_bson"  with the collections you want to indexate.
### v. 2.0
#               FILES: promos_without_bson  or  mcp_without_bson
## By Alan Muniz.

whereiam=$(pwd)
echo "What do you want to indexate? (promo / mcp) "
select yn in "promo" "mcp"; do
    case $yn in
        promo ) echo "indexaras promo";
                touch $whereiam/comandos-promo.sh
                rm $whereiam/comandos-promo.sh
                getArray()
                {
                array=()
                while IFS= read -r line
                do
                array+=("$line")
                done < "$1"
                }
                getArray "$whereiam/promos_without_bson"
                for collections in "${array[@]}"
                do
                ###
                # Index para visit_promo
                ###
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" _id \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" created_at \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" id_visitor \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" brand \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" location \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" attempt_date \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" optin_date \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" pixel_date \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" params.msisdn \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" params.ip \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" params.promo_id \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" params.attempt_id \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" params.publisher_id \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" params.publisher_usage \" : 1 },{ background : true } ) ' "
                echo "mongo tracking  --eval  'db.$collections.createIndex( { \" views.params.attempt_id \" : 1 },{ background : true } ) ' "
                done >> $whereiam/comandos-promo.sh
                bash $whereiam/comandos-promo.sh
                break;;

####################################################################################################################################

        mcp )   echo "indexaras mcp";
                touch $whereiam/comandos-mcp.sh
                rm $whereiam/comandos-mcp.sh
                getArray()
                {
                array=()
                while IFS= read -r line
                do
                array+=("$line")
                done < "$1"
                }
                getArray "$whereiam/mcp_without_bson"
                for collections in "${array[@]}"
                do
                ###
                # Index para visit_mcp
                ###
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" _id \" : 1 },{ background  : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" created_at \" : 1 },{ background : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" id_visitor \" : 1 },{ background : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" brand \" : 1 },{ background : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" location \" : 1 },{ background : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" params.msisdn \" : 1 },{ background : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" params.ip \" : 1 },{ background : true } ) ' "
                echo "mongo tracking   --eval   'db.$collections.createIndex( { \" params.user_token \" : 1 },{ background : true } ) ' "
                done >> $whereiam/comandos-mcp.sh
                bash $whereiam/comandos-mcp.sh
                break;;
    esac
done
