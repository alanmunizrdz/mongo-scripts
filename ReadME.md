`Mongo Scripts`

*Mongodump*

```
Remote mongo dump.
This will ask you for the IP,database and where you want to store the compress backup
The FILE "mongo_collections" should have the list of the collections you want to make the backup
example the name as: "visit_promo_XX_XX_XX" and should be where do you execute the script.
```

*Mongorestore*

```
With this script you can restore the collections one by one to the local-mongo
you should had use the script "mongodump.sh"
the file "mongo_bson" contains all the collections that you want to restore to the local mongo.
```

*Mongoindexer*

```
script to indexate the collections depending if are for visit_promos or visit_mcp
To considerate  inside the same folder you execute the script should exist the files
"promos_without_bson"  and "mcp_without_bson"  with the collections you want to indexate.
```
