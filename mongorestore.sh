#!/bin/bash
#
#   With this script you can restore the collections one by one to the local-mongo
#   you should had use the script "mongodump.sh"
#   the file "mongo_bson" contains all the collections that you want to restore to the local mongo.
#
#   Considerate: the file
#       By: Alan Muniz

whereiam=$(pwd)

echo -e "Path to mongodump:"
read -e -p "Route:" route

echo -e "Name of the database to restore"
read -e -p "Database:"  database

rm $whereiam/mongo_bson
cd $route
ls . | grep -Po '.*(?=\.bson)' | sort -u >> $whereiam/mongo_bson


getArray()
{
    array=()
    while IFS= read -r line
    do
        array+=("$line")
    done < "$1"
}
getArray "$whereiam/mongo_bson"

    for collections in "${array[@]}"
    do
        zcat $collections.bson.gz | mongorestore --collection $collections  --db $database -
    done 
