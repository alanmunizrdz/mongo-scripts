#!/bin/bash
#   Remote mongo dump.
#   This will ask you for the IP,database and where you want to store the compress backup

#   The FILE "mongo_collections" should have the list of the collections you want to make the backup
#   example the name as: "visit_promo_XX_XX_XX" and should be where do you execute the script.
#
#       By Alan Muniz.

whereiam=$(pwd)

echo -e "IP of remote host "
read -e -p "IP:" IP

echo -e "Which is the database: "
read -e -p "Database:" db

echo -e "where do you want to storage the backup? "
read -e -p "Backup path:" donde_se_guardan

getArray()
{
    array=()
    while IFS= read -r line
    do
        array+=("$line")
    done < "$1"
}
getArray "$whereiam/mongo_collections"

for collection in "${array[@]}"
do
    mongodump --host $IP --db=$db  --collection=$collection   --gzip  --out=$donde_se_guardan
done
